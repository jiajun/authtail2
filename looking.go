package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"
	"syscall"
	"time"

	"github.com/coreos/go-systemd/sdjournal"
	"github.com/go-telegram-bot-api/telegram-bot-api"
)

var (
	telegram_apikey            string
	telegram_allowed_usernames map[string]bool
	telegram_chat_ids          []int64
	start_time                 time.Time
)

type Config struct {
	TelegramApiKey           string
	TelegramAllowedUsernames []string
	TelegramChatIds          []int64
}

func loadConfig(filepath string) {
	log.Printf("Loading configuration from %s\n", filepath)
	jsonFile, err := os.Open(filepath)
	if err != nil {
		log.Fatal(err)
	}
	defer jsonFile.Close()
	byteValue, err := ioutil.ReadAll(jsonFile)
	if err != nil {
		log.Fatal(err)
	}
	var result Config
	json.Unmarshal([]byte(byteValue), &result)
	telegram_allowed_usernames = make(map[string]bool)
	telegram_chat_ids = make([]int64, 0)

	for _, u := range result.TelegramAllowedUsernames {
		telegram_allowed_usernames[u] = true
		fmt.Printf("Adding %s to allowed telegram users\n", u)
	}
	for _, c := range result.TelegramChatIds {
		telegram_chat_ids = append(telegram_chat_ids, c)
		fmt.Printf("adding %d to chat ids\n", c)
	}
	telegram_apikey = result.TelegramApiKey
	if len(telegram_apikey) == 0 {
		log.Fatal("Telegram API Key cannot be empty")
	}
}

func main() {
	start_time = time.Now()
	entryChan := make(chan *sdjournal.JournalEntry, 128)
	messageChan := make(chan string, 128)
	go tail(entryChan)
	go telegramBot(messageChan)
	messageChan <- fmt.Sprintf("[AuthTail Restarted]\n%s", getUptime())
	for {
		entry := <-entryChan
		entry_msg, ok := entry.Fields["MESSAGE"]
		if ok == false {
			continue
		}
		entry_syslog_identifier, ok := entry.Fields["SYSLOG_IDENTIFIER"]
		if ok == false {
			continue
		}
		if entry_syslog_identifier == "sshd" && !(strings.Contains(entry_msg, "pam_unix(sshd:session)") || strings.Contains(entry_msg, "Invalid user ")) {
			continue
		}
		if entry_syslog_identifier == "sshd" && strings.Contains(entry_msg, "pam_unix(sshd:session): session closed for user") {
			continue
		}
		if entry_syslog_identifier == "sudo" && !strings.Contains(entry_msg, "pam_unix(sudo:auth)") && !strings.Contains(entry_msg, "pam_unix(sudo:session)") {
			continue
		}
		if entry_syslog_identifier == "sudo" && strings.Contains(entry_msg, "session closed for user") {
			continue
		}
		if entry_syslog_identifier == "su" && !strings.Contains(entry_msg, "pam_unix(su:session)") {
			continue
		}
		messageChan <- entry_msg
		// log.Println(entry.Fields)
	}
}

func telegramBot(messageChan chan string) {
	loadConfig("config.json")
	bot, err := tgbotapi.NewBotAPI(telegram_apikey)
	if err != nil {
		log.Panic(err)
	}
	bot.Debug = false
	log.Printf("Authorized on account %s", bot.Self.UserName)
	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60
	updates, err := bot.GetUpdatesChan(u)
	go func() {
		for {
			msg := <-messageChan
			for _, c := range telegram_chat_ids {
				go func() {
					m := tgbotapi.NewMessage(c, msg)
					_, err = bot.Send(m)
				}()
			}
		}
	}()
	for {
		update := <-updates
		if update.Message == nil { // ignore any non-Message Updates
			continue
		}
		_, allowed := telegram_allowed_usernames[update.Message.From.UserName]
		if allowed == false {
			continue
		}
		cmd := update.Message.Command()
		if len(cmd) == 0 {
			continue
		}
		switch cmd {
		case "ping":
			go func() {
				uptime := getUptime()
				msg := tgbotapi.NewMessage(update.Message.Chat.ID, uptime)
				bot.Send(msg)
			}()
		}
	}
}

func tail(c chan *sdjournal.JournalEntry) {
	journal, err := sdjournal.NewJournal()
	if err != nil {
		log.Fatal(err)
	}
	defer journal.Close()
	match := sdjournal.Match{Field: "SYSLOG_FACILITY", Value: "4"}
	journal.AddMatch(match.String())
	match = sdjournal.Match{Field: "SYSLOG_FACILITY", Value: "10"}
	journal.AddMatch(match.String())
	match = sdjournal.Match{Field: "SYSLOG_IDENTIFIER", Value: "su"}
	journal.AddMatch(match.String())
	match = sdjournal.Match{Field: "SYSLOG_IDENTIFIER", Value: "sudo"}
	journal.AddMatch(match.String())
	match = sdjournal.Match{Field: "SYSLOG_IDENTIFIER", Value: "sshd"}
	journal.AddMatch(match.String())
	err = journal.SeekTail()
	if err != nil {
		log.Fatal(err)
	}
	for {
		r, _ := journal.Next()
		if r == 0 {
			break
		}
	}
	journal.Wait(sdjournal.IndefiniteWait)
	for {
		journal.Wait(sdjournal.IndefiniteWait)
		for {
			r, err := journal.Next()
			if err != nil {
				log.Fatal(err)
			}
			if r > 0 {
				entry, _ := journal.GetEntry()
				c <- entry
				continue
			}
			break
		}
	}
}

func getSysUptime() (time.Duration, error) {
	si := &syscall.Sysinfo_t{}
	err := syscall.Sysinfo(si)
	if err != nil {
		fmt.Println(err)
		return time.Duration(0), err
	}
	u := time.Duration(time.Second * time.Duration(si.Uptime))
	return u, nil

}

func getUptime() string {
	now := time.Now()
	sysUptime, err := getSysUptime()
	sysUptimeString := "-"
	if err == nil {
		sysUptimeString = sysUptime.String()
	}
	appUptime := now.Sub(start_time)
	t := fmt.Sprintf("System uptime: %s \nAuthTail uptime: %s", sysUptimeString, appUptime.String())
	return t
}
